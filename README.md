# unnamed_project

This project is aimed to create a library aimed at:
- Retrieving data about accounts, their followers from Twitter.
- Provide a friendly API to get statistics from obtained data.

## Contributing
Any contribution will be appreciated.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

## Status
WIP